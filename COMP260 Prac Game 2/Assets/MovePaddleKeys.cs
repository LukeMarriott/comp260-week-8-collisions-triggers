﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddleKeys : MonoBehaviour {

	private Rigidbody rigidbody;
	public float maxSpeed = 5.0f;
	public float force = 10.0f;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
	}
	
	// Update is called once per frame
	void Update () {
		
		Vector2 direction;
		direction.x = Input.GetAxis("Horizontal");
		direction.y = Input.GetAxis("Vertical");

		// scale by the maxSpeed parameter
		Vector2 velocity = direction * maxSpeed;

		// move the object
		rigidbody.AddForce(velocity.normalized * force);

	}
}
